/* jshint esnext: true */
/*
 * Le Tram de Nice
 * Copyright (C) 2018 Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

(function () {
  "use strict";

  // random number generators
  var random = {};
  random.LinearCongruentialEngine = class {
    constructor(seed, a, c, m) {
      this.a = a || 16807;
      this.c = c || 0;
      this.m = m || 2147483647;

      this.state = seed;
    }

    get() {
      this.state = (this.a * this.state + this.c)  % this.m;
      return this.state;
    }

    min() { return 0; }

    max() { return this.m; }
  };

  random.DefaultRandomEngine = random.LinearCongruentialEngine;

  random.UniformRealDistribution = class {
    constructor(a, b) {
      this.a = a;
      this.b = b;
    }

    get(rnd) {
      return this.constructor.getUniform(this.a, this.b, rnd);
    }

    static getUniform(min, max, rnd) {
      var r =  (rnd.get() - rnd.min()) / (rnd.max() - rnd.min()); // [0, 1)
      return r * (max - min) + min;
    }
  };

  random.uniform = new random.UniformRealDistribution(0, 1);
  random.NormalDistribution = class {
    constructor(mean, variance) {
      this.mean = mean;
      this.variance = variance;
    }

    get(rnd) {
      return this.constructor.getNormal(this.mean, this.variance, rnd);
    }

    static getNormal(mean, variance, rnd) {
      // Box-Muller transform
      if (this.constructor.z1) {
        var r = this.constructor.z1 * variance + mean;
        this.constructor.z1 = undefined;
        return r;
      }

      var u1 = 0, u2 = 0;
      while (0 === u1) u1 = random.uniform.get(rnd);
      while (0 === u2) u2 = random.uniform.get(rnd);

      var z0 = Math.sqrt(-2 * Math.log(u1)) * Math.cos(2*Math.PI * u2);
      this.constructor.z1 = Math.sqrt(-2 * Math.log(u1)) * Math.sin(2*Math.PI * u2);

      return z0 * variance + mean;
    }
  };

  var defaultRandom = new random.DefaultRandomEngine(1234);
  function randomGaussian(mean, variance) {
    return random.NormalDistribution.getNormal(mean, variance, defaultRandom);
  }

  var defaultRandomU = new random.DefaultRandomEngine(1234);
  function randomUniform(min, max) {
    return random.UniformRealDistribution.getUniform(min, max, defaultRandomU);
  }

  function timeMsToString(time) {
    return (Math.round(time/10)/100).toFixed(2) + " s";
  }

  const speedMax = 9; // m/s
  const acceleration = 8; // m/s²
  const tramLength = 32.5; // m
  const tramHeight = 3.27; // m
  const pantographHeight = 1.5; // m

  var onready = function(){};
  var isReady = false;
  var runWhenReady = function(f) {
    if (isReady) {
      f();
      return;
    }
    onready = f;
  };
  var tramImage = new Image();
  tramImage.onload = function() {
    isReady = true;
    onready();
  };

  tramImage.src = "tram.svg";

  class Bell {
    constructor() {
      this.context = new (window.AudioContext || window.webkitAudioContext)();
      this.volume = 1;
      this.type = "sine";
    }

    play(frequency) {
      if (!this.context) return;

      this.attack = 30;
      this.release = 240;

      var envelope = this.context.createGain();
      envelope.connect(this.context.destination);

      envelope.gain.setValueAtTime(0, this.context.currentTime);
      envelope.gain.setTargetAtTime(this.volume, this.context.currentTime, this.attack / 1000);
      envelope.gain.setTargetAtTime(0, this.context.currentTime + this.attack / 1000, this.release / 1000);

      var osc = this.context.createOscillator();

      osc.frequency.setValueAtTime(frequency, this.context.currentTime);
      osc.type = this.type;
      osc.connect(envelope);
      osc.start();

      osc.stop(this.context.currentTime + this.attack/1000 + this.release/1000+1.0);
    }
    time() {
      if (!this.context) return window.performance.now();

      return this.context.currentTime;
    }
  }

  class Pantograph {
    constructor() {
      this.setRaised();
    }

    draw(context) {
      if (this.raisedRatio > 0) {
        context.beginPath();
        context.moveTo(0, 0);

        const length = 1.5;
        const height = pantographHeight;
        var raisedAngle = Math.asin(this.raisedRatio * height / (2*length));
        context.lineTo(Math.cos(raisedAngle) * length, -Math.sin(raisedAngle) *length);
        context.lineTo(0, -2*Math.sin(raisedAngle) *length);
        context.lineWidth = 0.1;
        context.strokeStyle = "black";
        context.stroke();
      }
    }

    update(timeDelta) {
      if (this.movementDirection) {
        const velocity = 0.5;
        this.raisedRatio += Math.sign(this.movementDirection) * velocity * timeDelta;

        this.raisedRatio = Math.max(0, Math.min(this.raisedRatio, 1));
      }

      if (this.movementDirection < 0 && this.raisedRatio === 0) {
        this.movementDirection = 0;
      }
    }

    lower() {
      this.movementDirection = -1;
    }
    setRaised() {
      this.movementDirection = 0;
      this.raisedRatio = 1;
    }
  }

  const LEDCharMap = {
        "0":  " ... " +
              ".   ." +
              ".   ." +
              ".   ." +
              ".   ." +
              ".   ." +
              " ... ",

        "1":  "  .  " +
              " ..  " +
              "  .  " +
              "  .  " +
              "  .  " +
              "  .  " +
              " ... ",

        "2":  " ... " +
              ".   ." +
              "    ." +
              " ... " +
              ".    " +
              ".    " +
              ".....",

        "3":  " ... " +
              ".   ." +
              "    ." +
              "  .. " +
              "    ." +
              ".   ." +
              " ... ",

        "4":  "   . " +
              "  .. " +
              " . . " +
              "....." +
              "   . " +
              "   . " +
              "   . ",

        "5":  "....." +
              ".    " +
              ".    " +
              " ... " +
              "    ." +
              ".   ." +
              " ... ",

        "6":  " ... " +
              ".   ." +
              ".    " +
              ".... " +
              ".   ." +
              ".   ." +
              " ... ",

        "7":  "....." +
              "    ." +
              "   . " +
              "  .  " +
              " .   " +
              ".    " +
              ".    ",

        "8":  " ... " +
              ".   ." +
              ".   ." +
              " ... " +
              ".   ." +
              ".   ." +
              " ... ",

        "9":  " ... " +
              ".   ." +
              ".   ." +
              " ...." +
              "    ." +
              ".   ." +
              " ... ",

        ".":  "     " +
              "     " +
              "     " +
              "     " +
              "     " +
              "     " +
              "  .  ",

        "/":  "    ." +
              "   . " +
              "   . " +
              "  .  " +
              " .   " +
              " .   " +
              ".    ",

        "-":  "     " +
              "     " +
              "     " +
              "....." +
              "     " +
              "     " +
              "     ",

        "D":  "...  " +
              ".  . " +
              ".   ." +
              ".   ." +
              ".   ." +
              ".  . " +
              "...  ",

        "é":  "  .. " +
              "..   " +
              " ... " +
              ".   ." +
              "....." +
              ".    " +
              " ... ",

        "p":  "     " +
              "     " +
              ".... " +
              ".   ." +
              ".   ." +
              ".... " +
              ".    ",

        "a":  "     " +
              "     " +
              " ... " +
              "    ." +
              " ...." +
              ".   ." +
              " ....",

        "r":  "     " +
              "     " +
              ". .. " +
              "..  ." +
              ".    " +
              ".    " +
              ".    ",

        "t":  "     " +
              " .   " +
              "....." +
              " .   " +
              " .   " +
              " .  ." +
              "  .. ",

        "s":  "     " +
              "     " +
              " ...." +
              ".    " +
              " ... " +
              "    ." +
              ".... ",

        "m":  "     " +
              "     " +
              " . . " +
              ". . ." +
              ". . ." +
              ".   ." +
              ".   .",

        "\u25B6":  " .   " +
                   " ..  " +
                   " ... " +
                   " ...." +
                   " ... " +
                   " ..  " +
                   " .   ",

        "\u26a1":  "   . " +
                   "  .  " +
                   " ..  " +
                   "....." +
                   "  .. " +
                   "  .  " +
                   " .   ",
      };

  class LEDDisplay {
    constructor(numChars) {
      this.numChars = numChars;
      this.text = "";
      this.border = 2;
    }
    width() {
      return 6*this.numChars + 2*this.border;
    }
    height() {
      return 7 + 2*this.border;
    }
    setContext(context) {
      if (this.context !== context) {
        this.gradient = null;
      }
      this.context = context;

      if (!this.gradient) {
        this.gradient = context.createRadialGradient(0.5,0.5,0, 0.5,0.5,0.9);
        this.gradient.addColorStop(0, "white");
        this.gradient.addColorStop(0.3, "red");
        this.gradient.addColorStop(0.4, "red");
        this.gradient.addColorStop(0.8, "black");
      }
    }
    drawLED(x,y) {
      this.context.save();
      this.context.fillStyle = this.gradient;
      this.context.translate(x, y);
      this.context.fillRect(0, 0, 0.7, 0.7);
      this.context.restore();
    }
    drawChar(cx, cy, c) {
      var m = LEDCharMap[c];
      if (m) {
        for (var y = 0; y<7; y++) {
          for (var x = 0; x<5; x++) {
            if (m[5*y + x] != " ") {
              this.drawLED(6*cx + x, y + 8*cy);
            }
          }
        }
      }
    }

    clear() {
      var border = this.border;
      this.context.fillStyle = "black";
      this.context.translate(border, border);
      this.context.fillRect(-border, -border, 6*this.numChars +2*border, 7+2*border);
    }

    draw(context) {
      this.setContext(context);

      this.clear();

      this.context.fillStyle = this.gradient;
      for (var i = 0; i < this.text.length; ++i) {
        this.drawChar(i, 0, this.text[i]);
      }
    }
  }

  class OSD {
    constructor(game) {
      this.game = game;
      this.opacity = 0;
      this.opacityVelocity = 2.0;

      this.showInstructions = true;
      this.display = new LEDDisplay(20);
      this.bdisplay = new LEDDisplay(10);
    }

    update(timeDelta) {
      if (this.game.tram.isCharging()) {
        this.opacity += this.opacityVelocity * timeDelta;
        if (this.opacity >= 1) {
          this.opacity = 1;
          this.opacityVelocity = -this.opacityVelocity;
        } else if(this.opacity <= 0) {
          this.opacity = 0;
          this.opacityVelocity = -this.opacityVelocity;
        }
      }
    }

    draw(context) {
      var tram = this.game.tram;
      var width = context.canvas.width;
      var height = context.canvas.height;

      const topBorder = 0.7*height;

      context.fillStyle = "black";
      context.fillRect(0, topBorder, width, 1*height);
      context.fillStyle = "darkgrey";
      context.fillRect(0, topBorder, width, 0.01*height);

      // battery
      var batteryLength = 0.2 * width;
      var batteryHeight = 0.2 * batteryLength;

      context.save();
      context.translate(0.02*width, 1.05*topBorder);

      context.strokeStyle = tram.battery === 0 ? "red" :"#0f0";

      context.fillStyle = context.strokeStyle;
      context.fillRect(batteryLength, batteryHeight*0.2, batteryLength*0.1, batteryHeight*0.6);

      if (tram.battery < 0.1 && tram.battery > 0) {
        context.fillStyle = "orange";
      }
      context.fillRect(0,0, batteryLength * tram.battery, batteryHeight);

      if (tram.isCharging() && tram.battery < 1) {
        context.fillStyle = "hsl(120, 100%, "+(this.opacity*50)+"%)";
        context.font = (batteryHeight*0.8) + "px sans";
        context.textBaseline = "middle";
        context.textAlign = "center";
        context.fillText("\u26a1", batteryLength/2, batteryHeight/2, batteryLength/2);
      }

      context.beginPath();
      context.rect(0,0, batteryLength, batteryHeight);
      context.lineWidth = 0.02 * batteryLength;
      context.stroke();

      context.restore();

      // Distance
      context.save();
      var h = this.bdisplay.height();
      var displayScale = (height - 1.02*topBorder) / (2*h);
      displayScale = Math.min(displayScale,
        0.5 * width / this.bdisplay.width());
      context.translate(width - this.bdisplay.width()*displayScale, 1.02*topBorder);
      context.scale(displayScale,displayScale);
      if (this.showInstructions) {
        this.bdisplay.text = this.opacity > 0.5 ?  " Départ \u25B6\u25B6" : "";
      } else {
        const bpad = "          ";
        var distance = Math.round(tram.x) + " m";
        this.bdisplay.text = bpad.substring(0, bpad.length - distance.length) + distance;
      }
      this.bdisplay.draw(context);
      context.restore();

      // Time
      context.save();
      context.translate(width - this.display.width()*displayScale, 1.02*topBorder + h*displayScale);
      context.scale(displayScale,displayScale);
      const pad = "         ";
      var time = timeMsToString(this.game.elapsedTimeMs());
      if (tram.battery === 0) {
        time = "---.-- s";
      }
      this.display.text = pad.substring(0, pad.length - time.length) + time;
      if (this.game.previousBestTime) {
        var bestTime = timeMsToString(this.game.previousBestTime);
        this.display.text = pad.substring(0, pad.length - bestTime.length) +
          bestTime + " /" + this.display.text;
      } else {
        this.display.text = pad + "  " + this.display.text;
      }
      this.display.draw(context);
      context.restore();
    }
  }

  class Tram {
    // model: Alstom Citadis 302
    // ref: https://fr.wikipedia.org/wiki/Alstom_Citadis
    // http://cital-dz.com/produits-services/citadis
    // "Véhicules composés de 5 modules, 66% motorisés, bidirectionnels, climatisés, avec 3 bogies, exploitables en unité multiple. Longueur de 32,5 m - largeur de 2,650 m."
    // https://web.archive.org/web/20140326130343/http://tramwaynice.free.fr:80/Caracteristiques_techniques/Caracteristiques_Citadis_302_Alstom.htm

    constructor() {
      this.pantograph = new Pantograph();

      this.bell = new Bell();
      this.bell.type = "square";

      this.reset();
    }

    reset() {
      this.battery = 0; // %
      this.speed = 0; // m/s
      this.accelerationSign = 0;
      this.pantograph.setRaised();
    }

    ring() {
      this.bell.play(950);
    }

    stop() {
      this.speed = 0;
      this.accelerationSign = 0;
      this.pantograph.setRaised();
    }
    start() {
      if (this.battery > 0) {
        this.accelerationSign = 1;
      }
      this.pantograph.lower();
    }
    isCharging() {
      return this.accelerationSign === 0;
    }

    update(timeDelta) {
      var discargeRate = 0.05; // % points / s
      var chargeRate = 0.1 * 1/ Math.exp(this.battery); // % points / s
      // reduced rate for high battery to make being fast more risky
      if (this.isCharging()) {
        // charging
        this.battery += timeDelta * chargeRate;
        this.battery = Math.min(1, this.battery);
        return 0;
      }

      this.pantograph.update(timeDelta);

      this.speed += this.accelerationSign * acceleration * timeDelta;
      this.speed = Math.min(this.speed, speedMax);

      this.battery -= timeDelta * discargeRate * (this.speed / speedMax);
      this.battery = Math.max(0, this.battery);
      if (0 === this.battery) {
        return 0;
      }
      return this.speed * timeDelta;
    }

    draw(context) {
      var height = tramHeight; // m

      context.save();
      context.translate(tramLength/2, -height+0.1);
      this.pantograph.draw(context);
      context.restore();

      // shadow
      context.fillStyle = "black";
      context.fillRect(0.1*tramLength, 0, tramLength*0.9, 0.1);

      context.drawImage(tramImage, 0, -height, tramLength, height);
    }
  }


  class PalmTree {
    constructor() {
      this.angle = randomGaussian(0, 0.25);
      this.length = randomGaussian(4.5, 1.2); // m
      this.width = randomGaussian(0.4, 0.1);

      this.midLeafFactor = randomGaussian(0.7, 0.1);
      this.leafDownAngle = randomGaussian(0.5, 0.1);
      this.leafs = randomGaussian(7,0.1);
      this.leafScale = randomGaussian(1, 0.2);
    }
    draw(context) {
      context.beginPath();
      context.moveTo(0,0);
      var offset = Math.sin(this.angle) * this.length;
      var endY = -Math.cos(this.angle) * this.length;
      context.bezierCurveTo(0, -this.length/4, // controlpoint 1
        offset/3, -this.length*3/4, // cp 2
        offset, endY // end point
      );
      context.strokeStyle = "brown";
      context.lineWidth = this.width;
      context.lineCap = "butt";
      context.stroke();

      var polarLineTo = function (angle, length) {
        var centerX = offset;
        var centerY = endY;
        var x = Math.cos(angle) * length + centerX;
        var y = Math.sin(angle) * length + centerY;

        context.lineTo(x, y);
      };

      context.fillStyle = "green";

      var leafs = this.leafs;
      var innerLength = 0.5 * this.leafScale;
      var leafLength = 2 * this.leafScale;
      var leafMidLength = 1 * this.leafScale;

      var angle = 0;
      var midLeafAngle = 2*Math.PI/8*this.midLeafFactor;
      context.moveTo(offset, endY);
      context.beginPath();
      for (var i=0; i<leafs; i++) {
        polarLineTo(angle, innerLength);
        polarLineTo(angle, leafMidLength);
        if (angle < 1/4 * 2* Math.PI || angle > 3/4 *2* Math.PI ) {
          polarLineTo(angle+this.leafDownAngle, leafLength);
        } else  {
          polarLineTo(angle-this.leafDownAngle, leafLength);
        }
        polarLineTo(angle + midLeafAngle, leafMidLength);

        angle += 2*Math.PI/8;
      }
      context.fill();
    }
  }

  class FinishSign {
    draw(context) {
      const size = 5;
      const width = 10;
      context.fillStyle = "white";
      context.fillRect(-2*size-width, 0, 2*size + width, size);

      context.fillStyle = "black";
      context.font = "bold " + (0.9*size) + "px sans";
      context.textBaseline = "middle";
      context.textAlign = "center";
      context.fillText("Fin", -size - width/2, size/2);

      context.fillStyle = "black";
      context.translate(-size, 0);
      this.drawFlag(context, size);
      context.translate(-width-size, 0);
      this.drawFlag(context, size);
    }

    drawFlag(context, size) {
      for (var y = 0; y<size; ++y) {
        for (var x = 0; x<size; ++x) {
          context.fillStyle = (x+y) % 2 === 0 ? "black" : "white";
          context.fillRect(x,y,1,1);
        }
      }
    }
  }

  class StopSign {
    draw(context) {
      context.save();
      context.scale(1.2, 1.2);

      context.strokeStyle = "grey";
      context.lineWidth = 0.25;
      context.beginPath();
      context.moveTo(0,0);
      context.lineTo(0,-2.5);
      context.stroke();

      context.beginPath();
      context.fillStyle = "blue";
      context.rect(-0.6, -3.7, 1.2, 1.2);
      context.translate(0, -2.75);
      // T
      context.moveTo(-0.1, 0);
      context.lineTo(0.1, 0);
      context.lineTo(0.1, -0.4);
      context.lineTo(0.3, -0.4);
      context.lineTo(0.3, -0.5);

      context.lineTo(-0.3, -0.5);
      context.lineTo(-0.3, -0.4);
      context.lineTo(-0.1, -0.4);
      context.lineTo(-0.1, 0);

      context.fill();

      context.restore();
    }
  }

  class TramStop {
    constructor(isFinsih) {
      this.length = tramLength;
      this.sign = new StopSign();

      if(isFinsih) {
        this.finsih = new FinishSign();
      }
    }

    draw(context) {
      context.fillStyle = "grey";
      context.fillRect(0,-0.2, tramLength, 0.5);

      context.fillStyle = "black";
      context.fillRect(0, (-tramHeight -pantographHeight), tramLength, 0.01);


      context.translate(this.length, 0);
      this.sign.draw(context);

      if (this.finsih) {
        context.translate(0, -12);
        context.scale(0.8, 0.8);
        this.finsih.draw(context);
      }
    }
  }

  class Seagull {
    // length: 0.60 m
    // wingspan: 1.25 - 1.55 m
    // cord: ~ 0.4 m
    // bill: ~ 0.05 m
    constructor() {
      this.angle = randomUniform(Seagull.angleMax, Seagull.angleMin);
      this.height = -20;
      this.x = 0;
      this.xVelocity = randomUniform(4, 6);

      this.setFlapping();
    }

    setFlapping() {
      this.angularVelocity = this.xVelocity/5 * Seagull.angularVelocityFlapping;
    }

    update(timeDelta, length) {
      this.angle += this.angularVelocity * timeDelta;
      if (this.angle > Seagull.angleMax || this.angle < Seagull.angleMin) {
        this.angularVelocity = -this.angularVelocity;
        this.angle = Math.max(Seagull.angleMin, Math.min(this.angle, Seagull.angleMax));
      }

      const upProbability = 0.02;
      const downProbability = 0.3;

      const wingGlidingAngle = -0.3;
      const angleEpsilon = Math.abs(2.5 * timeDelta * this.angularVelocity);

      if (this.angularVelocity === 0) {
        // gliding
        if (Math.random() < upProbability) {
          this.setFlapping();
        }
      } else if (Math.abs(this.angle - wingGlidingAngle) < angleEpsilon) {
        if (Math.random() < downProbability) {
          this.angularVelocity = 0;
        }
      }

      const upVelocity = -0.2;
      const downVelocity = 0.1;

      if (this.angularVelocity !== 0) {
        // flapping
        this.height += 2 * upVelocity * timeDelta;
      } else {
        this.height += downVelocity * timeDelta;
      }

      this.x += this.xVelocity * timeDelta;
      if (length) {
        this.x = this.x % (1.5*length);
      }
    }

    draw(context) {
      context.save();
      context.translate(this.x, this.height);
      this.drawBody(context);
      this.drawWings(context, this.angle);
      context.restore();
    }

    drawBody(context) {
      context.fillStyle = "lightgrey";
      context.beginPath();
      context.moveTo(-0.4, 0);
      context.lineTo(0.1, -0.2);
      context.lineTo(0.4, 0);
      context.lineTo(-0.1, 0.2);
      context.lineTo(-0.4, 0);

      context.fill();
    }

    drawWings(context, angle) {
      context.scale(1, Math.sin(angle));
      if (angle < 0) {
        // upper side
        context.fillStyle = "darkgrey";
      } else {
        context.fillStyle = "lightgrey";
      }

      context.beginPath();
      context.moveTo(-0.2, -0.05);
      context.lineTo(-0.2, -0.8);
      context.lineTo(-0.3, -1.4);
      context.lineTo(0.2, -0.8);
      context.lineTo(0.2, -0.05);

      context.fill();
    }
  }
  Seagull.angularVelocityFlapping = 2.5;
  Seagull.angleMax = 0.5;
  Seagull.angleMin = -0.5;

  class Seagulls {
    constructor(length) {
      this.length = length;
      this.seagulls = [];
      for (var i = 0; i<7; i++) {
        this.seagulls[i] = new Seagull();
        this.seagulls[i].x = randomUniform(-0.5*length, 0.5*length);
        this.seagulls[i].height = -randomGaussian(20,4);
      }
    }
    update(timeDelta) {
      var length = this.length;
      this.seagulls.forEach(function(g) {
        g.update(timeDelta, length);
      });
    }

    draw(context) {
      this.seagulls.forEach(function(g) {
        g.draw(context);
      });
    }
  }

  class BackgroundTrees {
    constructor(length) {
      this.trees = [];
      for (var i = 0; i<length/10; i++) {
        this.trees[i] = new PalmTree();
        this.trees[i].x = randomGaussian(i*10, 5);
      }
      this.seagulls = new Seagulls(length);
    }

    update(timeDelta) {
      this.seagulls.update(timeDelta);
    }

    draw(context) {
      for (var i=0; i<this.trees.length; i++) {
        context.save();
        context.translate(this.trees[i].x, 0);
        this.trees[i].draw(context);
        context.restore();
      }
      this.seagulls.draw(context);
    }
  }

  class BackgroundBuildings {
    constructor(length, color, roofColor) {
      this.color = color;
      this.roofColor = roofColor;
      this.buildings = [];
      var prevX = 0;
      for (var i = 0; i<length/5; i++) {
        this.buildings[i] = {
          x: Math.max(prevX, prevX + randomGaussian(3, 5)),
          height: Math.max(3, randomGaussian(10,5)),
          width: Math.max(5, randomGaussian(15,8)),
          roofHeight: Math.max(0,randomGaussian(1.5,2)),
        };
        prevX = this.buildings[i].x + this.buildings[i].width;
      }

      this.y = 10;

      this.seagulls = new Seagulls(length);
    }

    update(timeDelta) {
      this.seagulls.update(timeDelta);
    }

    draw(context) {
      this.buildings.forEach(function(b) {
        context.save();
        context.translate(b.x, 0);
        context.fillStyle = this.color;
        context.fillRect(0, -b.height, b.width, b.height);

        if (b.roofHeight > 0) {
          context.fillStyle = this.roofColor;
          context.fillRect(0, -b.height-b.roofHeight, b.width, b.roofHeight);
        }
        context.restore();
      }.bind(this));

      this.seagulls.draw(context);
    }
  }

  class BackgroundHills {
    constructor(length) {
      this.peaks = [];
      var prevX = 0;
      for (var i = 0; i<length/10; i++) {
        this.peaks[i] = {
          x: Math.max(prevX, prevX + randomGaussian(8, 10)),
          y: Math.max(5, randomGaussian(18,7)),
        };
        prevX = this.peaks[i].x;
      }

      this.y = 100;
    }

    draw(context) {
      context.save();
      context.beginPath();
      context.moveTo(0,0);
      this.peaks.forEach(function(p) {
        context.lineTo(p.x, -p.y);
      });
      context.lineTo(this.peaks[this.peaks.length-1].x,0);
      context.lineTo(0,0);
      context.fillStyle = "#9277ff";
      context.fill();
      context.restore();
    }
  }


  class Game {
    constructor(canvas) {
      this.context = canvas.getContext("2d", {alpha: false});
      if (!this.context) {
        window.alert("getContext failed");
      }

      this.bell = new Bell();
      this.bell.volume = 0.01;

      this.tram = new Tram();

      var rnd = new random.DefaultRandomEngine(1234);
      var uniform = new random.UniformRealDistribution(40, 100);

      this.stops = [];
      var prevX = 0;
      this.stops[0] = new TramStop();
      this.stops[0].x = 0;
      const numStops = 10;
      for (var i=1; i<numStops; i++) {
        this.stops[i] = new TramStop(i === (numStops-1));
        this.stops[i].x = uniform.get(rnd) + prevX;
        prevX = this.stops[i].x;
      }
      this.backgrounds = [
        new BackgroundTrees(10*120),
        new BackgroundBuildings(10*120, "#d3d3d3", "#e28879"),
        new BackgroundBuildings(10*120, "#d3d3f5", "#e28888"),
        new BackgroundHills(10*120),
      ];
      this.backgrounds[0].pFactor = 1;
      this.backgrounds[1].pFactor = 0.9;
      this.backgrounds[2].pFactor = 0.8;
      this.backgrounds[3].pFactor = 0.7;

      this.backgrounds.sort(function (a,b) {
        if (a.pFactor < b.pFactor)
          return -1;
        if (a.pFactor > b.pFactor)
          return 1;
        return 0;
      });

      this.osd = new OSD(this);

      this.reset();
      this.begin = true;
    }
    reset() {
      this.ended = false;
      this.begin = false;
      this.completed = false;

      this.tram.reset();
      this.tram.x = 0;
      this.tram.nextStop = 1;

      this.view =  {
        x: 0,
        y: 3/5,
        lookAhead: true,
        speed: 0,
        accel: 5
      };

      this.timeStart = window.performance.now();
      this.isBestTime = false;
      this.previousBestTime = this.bestTimeMs();
    }

    elapsedTimeMs() {
      if (this.ended) {
        return this.ended - this.timeStart;
      }

      return window.performance.now()-this.timeStart;
    }

    bestTimeMs() {
      return localStorage.bestTimeMs;
    }
    setBestTimeMs(time) {
      localStorage.bestTimeMs = time;
    }

    update(timeDelta) {
      if (this.ended) {
        return;
      }

      this.tram.x += this.tram.update(timeDelta);

      if (this.tram.x >= this.stops[this.tram.nextStop].x) {
        this.view.lookAhead = true;
      }

      if (this.tram.x >= this.stops[this.tram.nextStop].x) {
        this.tram.stop();
        this.tram.x = this.stops[this.tram.nextStop].x;
        this.tram.nextStop++;
        if (this.tram.nextStop >= this.stops.length ) {
          this.tram.speed = 0;
          this.ended = window.performance.now();
          this.completed = true;
        }
      }
      this.osd.update(timeDelta);

      if (this.tram.battery === 1) {
        this.tramGo();
      }

      if (this.tram.battery === 0 && !this.tram.isCharging()) {
          this.ended = window.performance.now();
      }

      var targetX = this.tram.x - tramLength * 0.1;

      if (this.view.lookAhead) {
        targetX = this.tram.x + tramLength * 0.9;
      }

      this.backgrounds.forEach(function (background) {
        if (background.update) {
          background.update(timeDelta);
        }
      });

      const maxSpeed = 70;
      if (this.view.x < targetX) {
        this.view.speed += 0.1;
        this.view.x = Math.min(targetX, this.view.x + timeDelta * this.view.speed);
      } else {
        var delta = this.view.x - targetX;
        // lag such that speed matches as delta goes to 0
        const lagginess = 0.1;
        this.view.speed = Math.exp(-lagginess * delta) * this.tram.speed;
        this.view.x = Math.max(targetX, this.view.x + timeDelta * this.view.speed);
      }

      var tones = [800,900,1000,1100];
      if (!this.lastTone) this.lastTone = -100000000;

      var musicSpeed = 0.2 + 0.3 * this.tram.battery;
      if(this.lastTone + musicSpeed < this.bell.time() && this.tram.battery > 0) {
        var tone = tones[Math.floor(Math.random() * tones.length)];
        this.bell.play(tone);
        this.lastTone = this.bell.time();
      }
    }

    draw() {
      var context = this.context;
      context.setTransform(1, 0, 0, 1, 0, 0);
      context.fillStyle = "white";
      context.fillRect(0, 0, context.canvas.width, context.canvas.height);

      var scale = context.canvas.width / 120;  // pixels / m

      // track
      context.fillStyle = "black";
      context.fillRect(0, this.view.y*context.canvas.height, context.canvas.width, 2);

      // sky
      var gradient = context.createLinearGradient(0,0, 0, this.view.y*context.canvas.height);
      gradient.addColorStop(0, "blue");
      gradient.addColorStop(1, "white");
      context.fillStyle = gradient;
      context.fillRect(0, 0, context.canvas.width, this.view.y*context.canvas.height);

      context.translate(0, this.view.y*context.canvas.height);
      context.scale(scale, scale);

      this.backgrounds.forEach(function (background) {
        context.save();
        context.scale(background.pFactor, background.pFactor);
        context.translate(-this.view.x, 0);
        background.draw(context);
        context.restore();
      }.bind(this));

      context.translate(this.tram.x - this.view.x, 0);
      this.tram.draw(context);

      var view = this.view;
      this.stops.forEach(function(s) {
        context.setTransform(1, 0, 0, 1, 0, 0);
        context.translate(0, view.y*context.canvas.height);
        context.scale(scale, scale);
        context.translate(s.x - view.x, 0);
        s.draw(context);
      });

      context.setTransform(1, 0, 0, 1, 0, 0);
      this.osd.draw(context);

      if (this.begin) {
        var delta = window.performance.now() - this.timeStart;
        var opacity = Math.max(0.0, 1 - delta/1000);
        if (delta > 5000) {
          this.begin = false;
        }
        context.setTransform(1, 0, 0, 1, 0, 0);
        context.fillStyle = "rgba(255,255,255," + Math.max(0.0, 1 - delta/1000) +  ")";
        context.fillRect(0,0, context.canvas.width, context.canvas.height);

        context.fillStyle = "rgba(0,0,255," + Math.max(0.0, 1 - delta/5000) +  ")";
        context.font = (context.canvas.height*0.2) + "px serif";
        context.textBaseline = "middle";
        context.textAlign = "center";
        context.fillText("Le Tram de Nice", context.canvas.width/2, context.canvas.height/5);
        context.strokeStyle = "rgba(50,50,50," + Math.max(0.0, 1 - delta/5000) +  ")";
        context.lineWidth = context.canvas.height*0.005;
        context.strokeText("Le Tram de Nice", context.canvas.width/2, context.canvas.height/5);
      }

      if (this.ended) {
        context.setTransform(1, 0, 0, 1, 0, 0);

        context.fillStyle = "rgba(255,255,255," +
          Math.min(0.4, (window.performance.now() - this.ended)/1000) + ")";
        context.fillRect(0,0, context.canvas.width, context.canvas.height);

        context.fillStyle = "blue";
        context.font = (context.canvas.height*0.2) + "px serif";
        context.textBaseline = "middle";
        context.textAlign = "center";
        context.fillText("Le Tram de Nice", context.canvas.width/2, context.canvas.height/5);
        if (!this.begin) {
          context.font = (context.canvas.height*0.05) + "px serif";
          context.fillText("pour Sarah", context.canvas.width/2, context.canvas.height/5*1.5);
        }

        if (this.completed) {
          if (!this.bestTimeMs() || this.elapsedTimeMs() < this.bestTimeMs()) {
            this.setBestTimeMs(this.elapsedTimeMs());
            this.isBestTime = true;
          }
          if (this.isBestTime) {
            context.font = "bold " + (context.canvas.height*0.08) + "px serif";
            context.fillText("record: " + timeMsToString(this.elapsedTimeMs()),
              context.canvas.width/2, context.canvas.height/5*2.0);
          }
        } else {
          context.font = (context.canvas.height*0.08) + "px serif";
          context.fillText("fin" , context.canvas.width/2, context.canvas.height/5*2.5);
        }
      }

    }

    tramGo() {
      if (this.ended) {
        this.reset();
        return;
      }
      if (!this.timeStart) {
        this.timeStart = window.performance.now();
      }

      this.osd.showInstructions = false;
      this.tram.start();
      this.view.lookAhead = false;
      this.tram.ring();
    }

    updateAndDraw(timeDelta) {
      this.update(timeDelta);
      this.draw();
    }
  }

  runWhenReady(function () {
    var loading = document.getElementById("loading");
    loading.style.display = "none";


    var canvas = document.getElementById("tram_de_nice");

    var fitCanvas = function () {
      canvas.width = window.innerWidth;
      canvas.height = window.innerHeight;
    };
    fitCanvas();

    window.addEventListener("resize", function () {
      fitCanvas();
    });

    var game = new Game(canvas);
    game.draw();

    document.addEventListener("touchstart", function (e) {
      game.tramGo();
      e.preventDefault();
    });
    document.addEventListener("mousedown", function (e) {
      game.tramGo();
    });

    document.addEventListener("keydown", function (e) {
      if (e.key === " ") {
        game.tramGo();
      }
    });

    window.requestAnimFrame = (function (){
      return  window.requestAnimationFrame       ||
              window.webkitRequestAnimationFrame ||
              window.mozRequestAnimationFrame    ||
              function( callback ){
                window.setTimeout(callback, 1000 / 60);
              };
    })();


    var last;
    function step (timestamp) {
      var timeDeltaMs = last ? timestamp - last : 0;
      last = timestamp;
      game.updateAndDraw(timeDeltaMs / 1000);
      window.requestAnimationFrame( step );
    }
    window.requestAnimationFrame(step);
  });
})();
